The Cornipickle Bug Repository
==============================

This repository contains layout bugs in *real world* web applications
discovered as part of
the [Cornipickle](https://github.com/liflab/cornipickle)
project.

## Referring to this repository in your work

We are glad to share our resources and findings with you. 
As a courtesy to the developers, we ask that you **please cite the paper**
from the Journal of Logic and Algebraic Programming
describing the repository:

S. Hallé, N. Bergeron, F. Guérin, G. Le Breton, O. Beroual. (2016).
Declarative layout constraints for testing web applications.
In J. of  Logic and Algebraic Programming (JLAMP), 85(5): 737-758. DOI:
10.1016/j.jlamp.2016.04.001.

- [Full-text on ResearchGate](https://www.researchgate.net/publication/303495734_Declarative_layout_constraints_for_testing_web_applications)

## Repository contents

Cornipickle is a declarative tool for asserting and detecting
these bugs in web applications. It is being developed at the
[Laboratoire d'informatique formelle](http://liflab.ca) (LIF) by
Sylvain Hallé and a team of students. During this project, we performed a 
survey of more than 35 web applications over
a ten-month period in 2014-2015, and collected any bugs having an impact over
the layout or contents of their user interface. The sites were surveyed in an
informal way, by simply collecting data on bugs through the authors’ daily use
of the web. 

Every time such a bug was found, a bug report was created. This report
contains a short description of the bug (or the expected content of the page), a
screenshot of the offending part of the page, as well as a snapshot of the page’s
contents in the Mozilla Archive File Format (MAFF) or MIME HTML (MHT). In case
the page contains sensitive information (names, IDs, etc.), the pages have
been manually sanitized before uploading them to the repository.  It is possible
that some of the bugs mentioned in this repository have since been ﬁxed
in the corresponding site.


