<?php
include("patterns.inc.php");
/*
Remove user-specific strings from a page snapshot
*/

$tmpdir = tempdir();
echo "TMEP DIR: $tmpdir\n";
$command = "7z x ".$argv[1]." -o\"".$tmpdir."\"";
echo "The command: $command\n";
shell_exec($command);

//echo "PATH: $path";
$rep_cnt = 0;
$objects = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($tmpdir), RecursiveIteratorIterator::SELF_FIRST);
$last_dir = "";
foreach($objects as $name => $object)
{
    if (is_dir($name) && $name !== $tmpdir."/." && $name !== $tmpdir."/..")
    {
      $last_dir = $name;
    }
    if (!ends_with($name, ".jpg") && !ends_with($name, ".png"))
    {
      echo $name."\n";
      $contents = file_get_contents($name);
      for ($i = 0; $i < count($patterns); $i++)
      {
        $pat = $patterns[$i];
        $rep = $replacements[$i];
        $contents = preg_replace($pat, $rep, $contents, -1, $cnt);
        $rep_cnt += $cnt;
      }
      file_put_contents($name, $contents);
    }
}
echo $rep_cnt." replacement(s)\n";
$zip_name = str_replace("maff", "zip", $argv[2]);
$command = "7z a ".$zip_name." ".str_replace("/.", "", $last_dir);
exec($command);
exec("mv ".$zip_name." ".$argv[2]);

function tempdir($dir=false,$prefix='php') {
    $tempfile=tempnam(sys_get_temp_dir(),'');
    if (file_exists($tempfile)) { unlink($tempfile); }
    mkdir($tempfile);
    if (is_dir($tempfile)) { return $tempfile; }
}

function ends_with($s, $t)
{
  return substr($s, strlen($s) - strlen($t)) === $t;
}

?>
